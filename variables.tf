# REQUIRED VARIABLES

variable name {
  type        = string
  description = "Nombre del efs"
}

variable performance_mode {
  type        = string
  description = "Modo de performace del efs"
}

variable throughput_mode {
  type        = string
  description = "Modo de throughput del efs"
}

variable lifecycle_policy_transition {
  type        = string
  description = "Tiempo en dias para mandar a storage de poco acceso"
}

# OPTIONAL VARIABLES

variable mount_config {
  type = list(object({
    subnet          = string
    security_groups = list(string)
  }))

  description = "Configuracion punto de montaje del efs"
  default     = []
}

variable encrypted {
  type        = bool
  default     = true
  description = "La informacion se guarda encriptada en el disco"
}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar al efs"
  default     = null
}