# Modulo de terraform para crear efs en aws

### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| name  | Nombre del efs  | SI  | N/A  |
| performance_mode  | Modo de performace del efs  | SI  | N/A  |
| throughput_mode  | Modo de throughput del efs  | SI  | N/A  |
| lifecycle_policy_transition  | Tiempo en dias para mandar a storage de poco acceso  | SI  | N/A  |
| mount_config  | Configuracion punto de montaje del efs  | No  | []  |
| encrypted  | La informacion se guarda encriptada en el disco  | NO  | true  |
| tags  | Mapa de tags para asignar al efs  | NO  | null  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| arn  | Arn del efs creado  |
| id  | Id del efs creado  |
| dns_name  | Dns del efs creado  |