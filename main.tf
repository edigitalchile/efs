resource "aws_efs_file_system" "nfs" {


  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )

  lifecycle_policy {
    transition_to_ia = var.lifecycle_policy_transition
  }

  performance_mode = var.performance_mode

  throughput_mode = var.throughput_mode
  encrypted       = var.encrypted

}

resource "aws_efs_mount_target" "policies" {
  count           = length(var.mount_config)
  file_system_id  = aws_efs_file_system.nfs.id
  subnet_id       = var.mount_config[count.index].subnet
  security_groups = var.mount_config[count.index].security_groups

}