output "arn" {
  value       = aws_efs_file_system.nfs.arn
  description = "Arn del efs creado"
}

output "id" {
  value       = aws_efs_file_system.nfs.id
  description = "Id del efs creado"
}

output "dns_name" {
  value       = aws_efs_file_system.nfs.dns_name
  description = "Dns del efs creado"
}